# g5k-meta-packages

This repository contains meta-packages for all Debian-based environments.

## Making changes

All changes are generally made in the master branch. Changes are made to
the `generate-control` script, that generates the `debian/control` file.

Detailed process:
* Edit `generate-control` and make the changes at the most logical place.
  Try to keep this cleanly organized!
* Run: `./generate-control`
* Check the resulting changes using: `git diff`
* Commit the changes.
* At this point, you can also `git push` and check the gitlab CI result
* Edit `debian/changelog` and add your changes
* create a new tag and push it 
